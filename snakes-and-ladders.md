# Introduction
Snakes and Ladders is an ancient Indian board game regarded today as a worldwide classic. 
It is played between two or more players on a gameboard having numbered, gridded squares. 
A number of "ladders" and "snakes" are pictured on the board, each connecting two specific board squares

# Tasks
Your task is to make a simple class called SnakesAndLadders.
which will support Two Players game with 2 dice. Each dice roll will be an
integer from 1 to 6. The player will move the sum of die1 and die2.

# Board
Design your board to look like the following: 
https://raw.githubusercontent.com/adrianeyre/codewars/master/Ruby/Authored/snakesandladdersboard.jpg

# Rules
1.  There are two players and both start off the board on square 0.

2.  Player 1 starts and alternates with player 2.

3.  You follow the numbers up the board in order 1=>100

4.  If the value of both die are the same then that player will have another go.

5.  Climb up ladders. The ladders on the game board allow you to move upwards and get ahead faster. 
If you land exactly on a square that shows an image of the bottom of a ladder, 
then you may move the player all the way up to the square at the top of the ladder. (even if you roll a double).

6.  Slide down snakes. Snakes move you back on the board because you have to slide down them.
If you land exactly at the top of a snake, slide move the player all the way to the square 
at the bottom of the snake or chute. (even if you roll a double).

7.  Land exactly on the last square to win. The first person to reach the highest square on the board wins.
[Optional] Let's add a twist! If you roll too high, your player "bounces" off the last square and moves back. 
You can only win by rolling the exact number needed to land on the last square. For example, if you are on 
square 98 and roll a five, move your game piece to 100 (two moves), then "bounce" back to 99, 98, 97 (three, four then five moves.)

# Returns
Return Player n Wins!. Where n is winning player that has landed on square 100 without any remainding moves left.

Return Game over! if a player has won and another player tries to play.

Otherwise return Player n is on square x. Where n is the current player and x is the sqaure they are currently on.

# Hints if stuck
1. Start by modeling the Board. How will you represent the Squares? How will you represent Snakes and Ladders. If you think about it,
a snake just means that if you land on the square you move a specific negative amount regardless of the dice roll. A ladder means that 
you move a specific positive amount regardless of the dice.

2. Now that you have your board ready, think about how to incorporate players. What new classes do you need? Do you have a class that keeps
track of the entire game? Both the players and the Board state. To keep things simple for now, you can explicitly set the dice rolls.

3. [Optional] Let's incorporate randomness in the dice rolls. Go for it!


==========================================================
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class LadderSnakeGame
{
    
    
    
   
    public static void main (String [] args) throws IOException
    {
      
        
        BufferedReader myInput2 = new BufferedReader (new InputStreamReader (System.in));
      
       
        
        
        String sGame = "y"; 
        
        System.out.print ("Do you want to play? Y or N  >"); 
        sGame = myInput2.readLine (); 
        System.out.print ("\n\n\n\n\n\n");
        
        while (sGame.equals ("y"))
        {
            sGame = startGame(sGame); 
        }
       
        
        
    } 
    
    
    
    
    
   
    public static String startGame (String start) throws IOException
    {
        
        BufferedReader myInput = new BufferedReader (new InputStreamReader (System.in));
        
        
        
        
        int playerOne = 0;
        int playerTwo = 0;
        int diceRoll = 0; 
        int diceRoll2 = 0; 
        int playerOneRoll = 0; 
        int playerTwoRoll = 0; 
        String playAgain = "y"; 
        
        
        int snakesLaddersArray [] = new int [21]; 
        
        snakesLaddersArray [0] = 2;
        snakesLaddersArray [1] = 7;
        snakesLaddersArray [2] = 8;
        snakesLaddersArray [3] = 15;
        snakesLaddersArray [4] = 21;
        snakesLaddersArray [5] = 28;
        snakesLaddersArray [6] = 36;
        snakesLaddersArray [7] = 51;
        snakesLaddersArray [8] = 71;
        snakesLaddersArray [9] = 78;
        snakesLaddersArray [10] = 87;
        snakesLaddersArray [11] = 16;
        snakesLaddersArray [12] = 46;
        snakesLaddersArray [13] = 49;
        snakesLaddersArray [14] = 62;
        snakesLaddersArray [15] = 64;
        snakesLaddersArray [16] = 74;
        snakesLaddersArray [17] = 89;
        snakesLaddersArray [18] = 92;
        snakesLaddersArray [19] = 95;
        snakesLaddersArray [20] = 99;
       
        
        
        while (playAgain.equals ("y")) 
        {
            
            
            playerOneRoll =  getDice(diceRoll, diceRoll2); 
            playerTwoRoll =  getDice(diceRoll, diceRoll2);
            
         
            
            
            playerOne = playerOne + playerOneRoll;
            
            
            playerTwo = playerTwo + playerTwoRoll;
            
            
            
            
            playerOne = playerOneG(playerOne, playerOneRoll, snakesLaddersArray);
            
            playerTwo = playerTwoG(playerTwo, playerTwoRoll, snakesLaddersArray, playerOne);
            
            System.out.println("\t\t\t*************************************************************************");
            System.out.println ("\t\t\t*\t\t The player one currently on square " + playerOne + "\t\t\t*"); 
            System.out.println ("\t\t\t*\t\t The playerTwo is currently on square " + playerTwo + "\t\t*"); 
            System.out.println("\t\t\t*************************************************************************");
            
          
            if (playerOne == 100 || playerTwo == 100)
            {
                playerOne = 0;
                playerTwo = 0;
                
                System.out.print ("Do you want to play Again? Y or N     >  ");
                playAgain = myInput.readLine ();
                System.out.print ("\n\n\n\n\n\n\n\n\n\n\n\n");
            }
            else
            {
                
                System.out.print ("Do you want to play? Y or N     >  ");
                playAgain = myInput.readLine ();
                
            }
            
            
        }
        
        return playAgain; 
    }
    
    
    
    
    
  

   
    public static int getDice (int diceRoll, int diceRoll2)
    {
        diceRoll = (int)(Math.random()*6 )+1 ; 
        diceRoll2 = (int)(Math.random()*6 )+1 ;
        int move = diceRoll + diceRoll2; 
        return move; 
    }
    
    
    
    
   
    public static int playerOneG (int playerOne, int playerOneRoll, int snakesLaddersArray []) throws IOException 
    {
        
        
        if(playerOne == snakesLaddersArray[0]) 
        {
            playerOne = 38; 
            
        }
        else if (playerOne == snakesLaddersArray[1]) 
        {
            playerOne = 14;
            
            
        }
        else if (playerOne == snakesLaddersArray[2]) 
        {
            playerOne = 31; 
            
        }
        else if (playerOne == snakesLaddersArray[3]) 
        {
            playerOne = 26; 
           
            
        }
        else if (playerOne == snakesLaddersArray[4]) 
        {
            playerOne = 42; 
            
            
        }
        else if (playerOne == snakesLaddersArray[5])
        {
            
            
            playerOne = 84; 
            
        }
        else if (playerOne == snakesLaddersArray[6]) 
        {
            
            
            playerOne = 44; 
            
        }
        else if (playerOne == snakesLaddersArray[7]) 
        {
            
            
            playerOne = 67; 
        }
        else if (playerOne == snakesLaddersArray[8]) 
        {
            
            
            playerOne = 91; 
           
        }
        else if (playerOne == snakesLaddersArray[9]) 
        {
            
            
            playerOne = 98; 
           
        }else if (playerOne == snakesLaddersArray[10]) 
        {
            
            
            playerOne = 94; 
           
        }else if (playerOne == snakesLaddersArray[11]) 
        {
            
            
            playerOne = 6; 
           
        }else if (playerOne == snakesLaddersArray[12]) 
        {
            
            
            playerOne = 25; 
            
        }else if (playerOne == snakesLaddersArray[13]) 
        {
            
            
            playerOne = 11; 
            
        }else if (playerOne == snakesLaddersArray[14]) 
        {
            
            
            playerOne = 19; 
            
        }else if (playerOne == snakesLaddersArray[15]) 
        {
            
            
            playerOne = 60; 
           
        }else if (playerOne == snakesLaddersArray[16]) 
        {
            
            
            playerOne = 53; 
          
        }else if (playerOne == snakesLaddersArray[17]) 
        {
            
            
            playerOne = 68; 
            
        }else if (playerOne == snakesLaddersArray[18]) 
        {
            
            
            playerOne = 88;
           
        }else if (playerOne == snakesLaddersArray[19]) 
        {
            
            
            playerOne = 75; 
            
        }else if (playerOne == snakesLaddersArray[20]) 
        {
            
            
            playerOne = 80; 
           
        }
        if (playerOne < 0 || playerOne > 112) 
        {
            System.out.println ("An error has occured please reset the game!!!!!!");
        }
        
        else if (playerOne > 100) 
        {
        	int value=playerOne;
        	int diff=value-100;
        	int diff2=playerOneRoll-diff;
            playerOne = playerOne - playerOneRoll; 
            
            playerOne=playerOne+diff2-diff;
        }
        else if (playerOne == 100 )
        {
            System.out.println ("PLayer One WON"); 
            
        }
        
        
        
        return playerOne; 
    }
    
    
    
    
    
    
   
    
    public static int playerTwoG (int playerTwo, int playerTwoRoll, int snakesLaddersArray [], int playerOne) throws IOException
    {
        
        if(playerTwo == snakesLaddersArray[0])
        {
            playerTwo = 38;
           
            
            
        }
        else if (playerTwo == snakesLaddersArray[1])
        {
            playerTwo = 14;
            
            
        }
        else if (playerTwo == snakesLaddersArray[2])
        {
            playerTwo = 31;
           
        }
        else if (playerTwo == snakesLaddersArray[3])
        {
            playerTwo = 26;
            
        }
        else if (playerTwo == snakesLaddersArray[4])
        {
            playerTwo = 42;
           
            
        }
        else if (playerTwo == snakesLaddersArray[5])
        {
            playerTwo = 84;
           
        }
        else if (playerTwo == snakesLaddersArray[6])
        {
            playerTwo = 44;
            
        }else if (playerTwo == snakesLaddersArray[7])
        {
            playerTwo = 67;
            
        }else if (playerTwo == snakesLaddersArray[8])
        {
            playerTwo = 91;
            
        }else if (playerTwo == snakesLaddersArray[9])
        {
            playerTwo = 98;
          
        }else if (playerTwo == snakesLaddersArray[10])
        {
            playerTwo = 94;
            
        }else if (playerTwo == snakesLaddersArray[11])
        {
            playerTwo = 6;
            
        }else if (playerTwo == snakesLaddersArray[12])
        {
            playerTwo = 25;
           
        }else if (playerTwo == snakesLaddersArray[13])
        {
            playerTwo = 11;
            
        }else if (playerTwo == snakesLaddersArray[14])
        {
            playerTwo = 19;
            
        }else if (playerTwo == snakesLaddersArray[15])
        {
            playerTwo = 60;
            
        }else if (playerTwo == snakesLaddersArray[16])
        {
            playerTwo = 53;
            
        }else if (playerTwo == snakesLaddersArray[17])
        {
            playerTwo = 68;
           
        }else if (playerTwo == snakesLaddersArray[18])
        {
            playerTwo = 88;
            
        }else if (playerTwo == snakesLaddersArray[19])
        {
            playerTwo = 75;
            
        }else if (playerTwo == snakesLaddersArray[20])
        {
            playerTwo = 80;
            
        }
        
        if (playerTwo < 0 || playerTwo > 112) 
        {
            System.out.println ("An error has occured for the PlayerTwo, please reset the game!!!!!!");
        }
        
        else if (playerTwo > 100) 
        {
        	int value=playerTwo;
        	int diff=value-100;
        	int diff2=playerTwoRoll-diff;
        	
            playerTwo = playerTwo - playerTwoRoll;
            
            playerTwo=playerTwo+diff2-diff;
            
            
        }
        else if (playerTwo == 100  && playerOne != 100)
        {
            System.out.println ("PlayerTwo Won, PlayerOne Failed!!!"); 
            
        }
        
        return playerTwo;
    } 
}



































